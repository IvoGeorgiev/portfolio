from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def home_page(request):
    # return HttpResponse('Hello World')
    return render(request, 'index.html')

def match_score(request):
    # return HttpResponse('Hello World')
    return render(request, 'match-score.html')

def forum_system_api(request):
    return render(request, 'forum-system-api.html')

def django_forum(request):
    return render(request, 'django-forum.html')

def portfolio_site(request):
    return render(request, 'portfolio-site.html')