from django.urls import path
from . import views

urlpatterns = [
    path('', views.home_page),
    path('match-score/', views.match_score),
    path('forum-system-api/', views.forum_system_api),
    path('django-forum/', views.django_forum),
    path('portfolio-site/', views.portfolio_site),
]
